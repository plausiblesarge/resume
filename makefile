

.PHONY: standard, print, coverletter, clean

standard:
	pdflatex resume.tex

print:
	pdflatex "\def\isprint{1} \input{resume.tex}"

coverletter:
	pdflatex cover_letter.tex

clean:
	rm *.aux *.log *.pdf *.out
